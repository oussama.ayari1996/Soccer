import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PlayersService } from 'src/app/services/players.service';
import { TeamsService } from 'src/app/services/teams.service';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {

  addPlayerForm!: FormGroup;
  player: any = {}; // Initialize with an empty player object
  id: any;
  title: string = "Add Player";
  teams:any=[];

  constructor(private router: Router, private activatedRoute: ActivatedRoute,private mService:PlayersService, private tService:TeamsService) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get("id");
    if (this.id) {
      this.title = "Update Player";
      this.getPlayerById();
    }
  }

  addPlayer() {
    let players = JSON.parse(localStorage.getItem('players') || "[]");
    
    if (this.id) {
     this.mService.editPlayer(this.player).subscribe();
    }
    else {
     this.mService.addPlayer(this.player).subscribe();

    }

    localStorage.setItem("players", JSON.stringify(players));
    this.router.navigate(["/tableplayers"]);
  }

  generateId(players: any) {
    if (players.length === 0) {
      return 1;
    } else {
      let max = 0;
      for (let i = 0; i < players.length; i++) {
        if (max < players[i].id) {
          max = players[i].id;
        }
      }
      return max + 1;
    }
  }

  getPlayerById() {
    let players = JSON.parse(localStorage.getItem('players') || '[]');
    for (let i = 0; i < players.length; i++) {
      if (players[i].id === Number(this.id)) {
        this.player = players[i];
      }
    }
  }
}
