import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlayersService {
  playersUrl = "http://localhost:8080/api/players";

  constructor(private http: HttpClient) { }

  addPlayer(player: any) { 
    return this.http.post<{ player: any }>(this.playersUrl, player);
  }

  editPlayer(player: any) { 
    return this.http.put<{ player: any }>(`${this.playersUrl}/${player.id}`, player);
  }

  getPlayerById(id: any) {
    return this.http.get<{ player: any }>(`${this.playersUrl}/${id}`);
  }

  deletePlayerById(id: any) {
    return this.http.delete<{ player: any }>(`${this.playersUrl}/${id}`);
  }

  getAllPlayers() {
    return this.http.get<{ players: any[] }>(this.playersUrl);
  }
}

