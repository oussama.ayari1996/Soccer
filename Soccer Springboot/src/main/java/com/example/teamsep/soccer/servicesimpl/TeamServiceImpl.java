package com.example.teamsep.soccer.servicesimpl;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.teamsep.soccer.models.TeamModel;
import com.example.teamsep.soccer.service.TeamService;
import com.example.teamsep.soccer.repositories.TeamRepository;

@Service
public class TeamServiceImpl implements TeamService{
	@Autowired
	private TeamRepository teamRepo;
	
	@Override
	public List<TeamModel> getAllTeams(){
		// TODO Auto-generated method stub
		return teamRepo.findAll();
	}

	@Override
	public TeamModel getTeamsById(Long id) {
		Optional<TeamModel> t = teamRepo.findById(id);
		
		return t.isPresent()? t.get():null;
	}

	@Override
	public void deleteTeamById(Long id) {
		// TODO Auto-generated method stub
		teamRepo.deleteById(id);
	}

	@Override
	public TeamModel addTeam(TeamModel t) {
		// TODO Auto-generated method stub
		return teamRepo.save(t);
	}

	@Override
	public TeamModel editTeam(TeamModel t) {
		// TODO Auto-generated method stub
		return teamRepo.save(t);
	}
	
}
