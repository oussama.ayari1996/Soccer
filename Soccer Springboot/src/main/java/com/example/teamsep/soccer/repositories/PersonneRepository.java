package com.example.teamsep.soccer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.teamsep.soccer.models.Personne;

public interface PersonneRepository extends JpaRepository<Personne, Long> {

}
