package com.example.teamsep.soccer.servicesimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.teamsep.soccer.models.PlayerModel;

import com.example.teamsep.soccer.repositories.PlayerRepository;
import com.example.teamsep.soccer.service.PlayerService;

@Service
public class PlayerServiceImpl implements PlayerService {

	@Autowired
	private PlayerRepository playerRepo;
	@Override
	public List<PlayerModel> getAllPlayers() {
		// TODO Auto-generated method stub
		return playerRepo.findAll();
	}

	@Override
	public PlayerModel getPlayerById(Long id) {
Optional<PlayerModel> p = playerRepo.findById(id);
		
		return p.isPresent()? p.get():null;
	}

	@Override
	public void deletePlayerById(Long id) {
		// TODO Auto-generated method stub
		playerRepo.deleteById(id);
	}

	@Override
	public PlayerModel addPlayer(PlayerModel p) {
		// TODO Auto-generated method stub
		return playerRepo.save(p);
	}

	@Override
	public PlayerModel editPlayer(PlayerModel p) {
		// TODO Auto-generated method stub
		return playerRepo.save(p);
	}

}
