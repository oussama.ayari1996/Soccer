package com.example.teamsep.soccer.models;

import javax.persistence.Entity;
import javax.persistence.Column;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table (name="teams")
public class TeamModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name="TEAM_ID")
private Long id;
	
private String country;
private String name;
private String owner;
private String league;
public TeamModel() {
	
	
	
}
public TeamModel(String country, String name, String owner, String league) {
	super();
	this.country = country;
	this.name = name;
	this.owner = owner;
	this.league = league;
}
public Long getId() {
	return id;
}

public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getOwner() {
	return owner;
}
public void setOwner(String owner) {
	this.owner = owner;
}
public String getLeague() {
	return league;
}
public void setLeague(String league) {
	this.league = league;
}
@Override
public String toString() {
	return "TeamModel [id=" + id + ", country=" + country + ", name=" + name + ", owner=" + owner + ", league=" + league
			+ "]";
}







}
