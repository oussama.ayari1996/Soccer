package com.example.teamsep.soccer.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.teamsep.soccer.models.PlayerModel;
import com.example.teamsep.soccer.service.PlayerService; 

@RequestMapping("api/players")
@CrossOrigin("*")
@RestController
public class PlayerController {
    @Autowired
    private PlayerService playerService; 

    @GetMapping
    public List<PlayerModel> getAllPlayers() {
        return playerService.getAllPlayers();
    }

    @GetMapping("/{id}")
    public PlayerModel getPlayer(@PathVariable Long id) {
        return playerService.getPlayerById(id);
    }

    @DeleteMapping("/{id}")
    public void deletePlayerById(@PathVariable Long id) {
        playerService.deletePlayerById(id);
    }

    @PostMapping
    public PlayerModel addPlayer(@RequestBody PlayerModel player) {
        return playerService.addPlayer(player);
    }

    @PutMapping
    public PlayerModel editPlayer(@RequestBody PlayerModel player) {
        return playerService.editPlayer(player);
    }
}

