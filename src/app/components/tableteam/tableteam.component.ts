import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TeamsService } from 'src/app/services/teams.service';

@Component({
  selector: 'app-tableteam',
  templateUrl: './tableteam.component.html',
  styleUrls: ['./tableteam.component.css']
})
export class TableteamComponent implements OnInit {
  teams: any = [];

  constructor(private router: Router ,private mService: TeamsService) { }

  ngOnInit() {
    this.mService.getAllTeams().subscribe((response)=>{this.teams=response;})

    //this.getAllTeams();
  }

 // getAllTeams() {


    //this.teams = JSON.parse(localStorage.getItem('teams') || "[]");
 // }

  deleteTeam(id: any) {
    for (let i = 0; i < this.teams.length; i++) {
      if (this.teams[i].id === id) {
        this.teams.splice(i, 1);
      }
    }
    localStorage.setItem("teams", JSON.stringify(this.teams));
  }

  navigate(id: any) {
    this.router.navigate(["teams/" + id]); // Navigate to the add-teams route with the team ID
  }
}
