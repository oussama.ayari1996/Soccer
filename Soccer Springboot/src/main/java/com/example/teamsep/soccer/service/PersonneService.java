package com.example.teamsep.soccer.service;

import java.util.List;

import com.example.teamsep.soccer.models.Personne;


public interface PersonneService {
	public List<Personne> getAllPersonne();
	public Personne getPersonneById(Long id);
	public void deletePersonneById(Long id);
	public Personne addPersonne(Personne p);
	public Personne editPersonne(Personne p);
}
