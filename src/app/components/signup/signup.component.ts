import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from '../matchPwd';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupForm:FormGroup

  constructor(private fb:FormBuilder) { }

  ngOnInit() {
    this.signupForm=this.fb.group({

      firstName:['' , [Validators.minLength(3), Validators.required]],
      email: ['', [Validators.email, Validators.required]],
      password: [''],
      confirmPassword: [''],
    },{
      validators:MustMatch("password","confirmPassword")
    })
    
  }
signup(){
console.log("here user",this.signupForm.value); 

}


}
