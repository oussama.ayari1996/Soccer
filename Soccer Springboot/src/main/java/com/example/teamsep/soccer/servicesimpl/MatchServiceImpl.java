package com.example.teamsep.soccer.servicesimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.teamsep.soccer.models.MatchModel;
import com.example.teamsep.soccer.repositories.MatchRepository;
import com.example.teamsep.soccer.service.MatchService;

@Service
public class MatchServiceImpl implements MatchService {
@Autowired
private MatchRepository matchRepo;
	
	
	@Override
	public List<MatchModel> getAllMatches() {
		// TODO Auto-generated method stub
		return matchRepo.findAll();
	}

	@Override
	public MatchModel getMatchById(Long id) {
		Optional<MatchModel> m = matchRepo.findById(id);
		
		return m.isPresent()? m.get():null;
	}

	@Override
	public void deleteMatchById(Long id) {
		matchRepo.deleteById(id);
		
	}

	@Override
	public MatchModel addMatch(MatchModel m) {
		// TODO Auto-generated method stub
		return matchRepo.save(m);
	}

	@Override
	public MatchModel editMatch(MatchModel m) {
		
		return matchRepo.save(m);
	}

}
