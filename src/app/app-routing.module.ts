import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ContactComponent } from './components/contact/contact.component';
import { AddMatchComponent } from './components/add-match/add-match.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { TableMatchesComponent } from './components/table-matches/table-matches.component';
import { DiversComponent } from './components/match/divers.component';
import { PlayersComponent } from './components/players/players.component';
import { TableplayersComponent } from './components/tableplayers/tableplayers.component';
import { TeamsComponent } from './components/teams/teams.component';
import { TableteamComponent } from './components/tableteam/tableteam.component';
import { MatchesComponent } from './components/matches/matches.component';
import { MatchDetailComponent } from './components/match-detail/match-detail.component';


const routes: Routes = [

  {path:"", component:HomeComponent},
  {path:"contact",component:ContactComponent},
  {path:"add-match",component:AddMatchComponent},
  {path:"add-match/:id",component:AddMatchComponent},
  {path:"login",component:LoginComponent},
  {path:"signup",component:SignupComponent},
  {path:"table-matches",component:TableMatchesComponent},
  {path:"divers",component:DiversComponent},
  {path:"players",component:PlayersComponent},
  {path:"players/:id",component:PlayersComponent},
  {path:"tableplayers",component:TableplayersComponent},
  {path:"teams",component:TeamsComponent},
  {path:"tableteam",component:TableteamComponent},
  {path:"teams/:id",component:TeamsComponent},
  {path:"matches",component:MatchesComponent},
  {path:"match-detail",component:MatchDetailComponent},
  { path: 'match-detail/:id', component: MatchDetailComponent },
  
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
