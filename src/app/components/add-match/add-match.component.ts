import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatchesService } from 'src/app/services/matches.service';


@Component({
  selector: 'app-add-match',
  templateUrl: './add-match.component.html',
  styleUrls: ['./add-match.component.css']
})
export class AddMatchComponent implements OnInit {
  addMatchForm!: FormGroup
  match: any = { }
  id: any
  title: string = "add match"
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private mService:MatchesService) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get("id")
    if (this.id) {
      this.title = "update Match";
      this.getMatchById()
    }
  }
  addMatch() {
    //let T = JSON.parse(localStorage.getItem('matches') || "[]")
    if (this.id) {
     this.mService.editMatch(this.match).subscribe( (response)=> {console.log("here response after edit",response)});
    }
    else {
     this.mService.addMatch(this.match).subscribe();

    }

    //localStorage.setItem("matches", JSON.stringify(T));
    this.router.navigate(["/table-matches"])
  }



  generateId(matches: any) {
    if (matches.length === 0) {
      return 1;
    } else {
      let max = 0

      for (let i = 0; i < matches.length; i++) {
        if (max < matches[i].id) {
          max = matches[i].id
        }
      }
      return max + 1
    }

  }
  
  getMatchById() {
this.mService.getMatchById(this.id).subscribe( (response)=> {console.log("here match by id",response);
this.match=response});

    //let matches = JSON.parse(localStorage.getItem('matches') || '[]')
   // for (let i = 0; i < matches.length; i++) {
    //  if (matches[i].id === Number(this.id)) {
       // this.match = matches[i]
     // }

   // }
  }
}
