package com.example.teamsep.soccer.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.teamsep.soccer.models.TeamModel;
import com.example.teamsep.soccer.service.TeamService;

@RequestMapping("api/teams") 
@CrossOrigin("*")
@RestController
public class TeamController {
    @Autowired
    private TeamService teamService;

    @GetMapping
    public List<TeamModel> getAllTeams() {
        return teamService.getAllTeams();
    }

    @GetMapping("/{id}")
    public TeamModel getTeam(@PathVariable Long id) {
        return teamService.getTeamsById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteTeamById(@PathVariable Long id) {
        teamService.deleteTeamById(id);
    }

    @PostMapping
    public TeamModel addTeam(@RequestBody TeamModel team) {
        return teamService.addTeam(team);
    }

    @PutMapping
    public TeamModel editTeam(@RequestBody TeamModel team) {
        return teamService.editTeam(team);
    }
}
