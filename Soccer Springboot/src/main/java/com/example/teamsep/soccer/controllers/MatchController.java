package com.example.teamsep.soccer.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.teamsep.soccer.models.MatchModel;
import com.example.teamsep.soccer.service.MatchService;

@RequestMapping("api/matches")
@CrossOrigin("*")
@RestController
public class MatchController {
	@Autowired
	private MatchService matchService;
	
	@GetMapping
	public List<MatchModel> getAllMatches(){
	return matchService.getAllMatches();
	}
	
	@GetMapping("/{id}")
	public MatchModel getMatch(@PathVariable Long id) {
		
		return matchService.getMatchById(id);
	}
	@DeleteMapping("/{x}")
	public void deleteMatchById(@PathVariable Long x) {
		matchService.deleteMatchById(x);
	}
	
	@PostMapping
	public MatchModel addMatch(@RequestBody MatchModel m) {
	return matchService.addMatch(m); }

	@PutMapping
	public MatchModel editMatch(@RequestBody MatchModel m) {
		return matchService.editMatch(m);
	}
	
}
