package com.example.teamsep.soccer.service;

import java.util.List;

import com.example.teamsep.soccer.models.PlayerModel;


public interface PlayerService {

	
	public List<PlayerModel> getAllPlayers();
	public PlayerModel getPlayerById(Long id);
	public void deletePlayerById(Long id);
	public PlayerModel addPlayer(PlayerModel p);
	public PlayerModel editPlayer(PlayerModel p);
}
