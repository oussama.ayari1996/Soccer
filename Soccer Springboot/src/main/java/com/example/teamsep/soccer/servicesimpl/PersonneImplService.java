package com.example.teamsep.soccer.servicesimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.teamsep.soccer.repositories.PersonneRepository;
import com.example.teamsep.soccer.service.PersonneService;
import com.example.teamsep.soccer.models.Personne;

@Service
public class PersonneImplService implements PersonneService{

	
	@Autowired
	private PersonneRepository PersonneRepo;
	@Override
	public List<Personne> getAllPersonne() {
		return PersonneRepo.findAll();
	}

	@Override
	public Personne getPersonneById(Long id) {
Optional<Personne> p = PersonneRepo.findById(id);
		
		return p.isPresent()? p.get():null;
	}

	@Override
	public void deletePersonneById(Long id) {
		PersonneRepo.deleteById(id);
		
	}

	@Override
	public Personne addPersonne(Personne p) {
		return PersonneRepo.save(p);
	}

	@Override
	public Personne editPersonne(Personne p) {
		return PersonneRepo.save(p);
	}

}
