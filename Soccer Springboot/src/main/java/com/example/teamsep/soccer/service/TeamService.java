package com.example.teamsep.soccer.service;

import java.util.List;



import com.example.teamsep.soccer.models.TeamModel;


public interface TeamService {
	
	public List<TeamModel> getAllTeams();
	public TeamModel getTeamsById(Long id);
	public void deleteTeamById(Long id);
	public TeamModel addTeam(TeamModel t);
	public TeamModel editTeam(TeamModel t);

}
