package com.example.teamsep.soccer.service;

import java.util.List;



import com.example.teamsep.soccer.models.MatchModel;



public interface MatchService {

	public List<MatchModel> getAllMatches();
	public MatchModel getMatchById(Long id);
	public void deleteMatchById(Long id);
	public MatchModel addMatch(MatchModel m);
	public MatchModel editMatch(MatchModel m);
	
	
}
