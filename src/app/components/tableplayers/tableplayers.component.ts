import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PlayersService } from 'src/app/services/players.service';

@Component({
  selector: 'app-tableplayers',
  templateUrl: './tableplayers.component.html',
  styleUrls: ['./tableplayers.component.css']
})
export class TableplayersComponent implements OnInit {
  players: any = [];

  constructor(private router: Router, private mService:PlayersService) { }

  ngOnInit() {
    this.mService.getAllPlayers().subscribe((response)=>{this.players=response;})
    //this.getAllPlayers();
  }

  //getAllPlayers() {
   // this.players = JSON.parse(localStorage.getItem('players') || "[]");
 // }

  deletePlayer(id: any) {
    for (let i = 0; i < this.players.length; i++) {
      if (this.players[i].id === id) {
        this.players.splice(i, 1);
      }
    }
    localStorage.setItem("players", JSON.stringify(this.players));
  }

  navigate(id: any) {
    this.router.navigate(["/players/" + id]);
  }
}
