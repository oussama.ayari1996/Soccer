import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MatchesService {
  matchUrl = "http://localhost:8080/api/matches";

  constructor(private http: HttpClient) { }

  addMatch(match: any) { 
    return this.http.post<{ data: any }>(this.matchUrl, match);
  }

  editMatch(match: any) { 
    return this.http.put<{ data: any }>(this.matchUrl, match);
  }

  getMatchById(id: any) {
    return this.http.get<{ x: any }>(`${this.matchUrl}/${id}`);
  }

  deleteMatchById(id: any) {
    return this.http.delete<{ data: any }>(`${this.matchUrl}/${id}`);
  }

  getAllMatches() {
    return this.http.get<{ matches: any }>(this.matchUrl);
  }
}
