import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {
  teamsUrl = "http://localhost:8080/api/teams";

  constructor(private http: HttpClient) { }

  addTeam(team: any) { 
    return this.http.post<{ team: any }>(this.teamsUrl, team);
  }

  editTeam(team: any) { 
    return this.http.put<{ team: any }>(`${this.teamsUrl}/${team.id}`, team);
  }

  getTeamById(id: any) {
    return this.http.get<{ team: any }>(`${this.teamsUrl}/${id}`);
  }

  deleteTeamById(id: any) {
    return this.http.delete<{ team: any }>(`${this.teamsUrl}/${id}`);
  }

  getAllTeams() {
    return this.http.get<{ teams: any[] }>(this.teamsUrl);
  }
}
