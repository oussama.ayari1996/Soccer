package com.example.teamsep.soccer.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table (name="players")
public class PlayerModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name="PLAYER_ID")
	private Long id;
	private String playerName;
	private String team;
	private String scoreNumber;
	private int age;
	public PlayerModel() {
		
	}
	
	public PlayerModel(String playerName, String team, String scoreNumber, int age) {
		super();
		this.playerName = playerName;
		this.team = team;
		this.scoreNumber = scoreNumber;
		this.age = age;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public String getScoreNumber() {
		return scoreNumber;
	}
	public void setScoreNumber(String scoreNumber) {
		this.scoreNumber = scoreNumber;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "PlayerModel [id=" + id + ", playerName=" + playerName + ", team=" + team + ", scoreNumber="
				+ scoreNumber + ", age=" + age + "]";
	}
	
	
	
	
}
