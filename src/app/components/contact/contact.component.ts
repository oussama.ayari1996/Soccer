import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  count: number = 0

  constructor() { }

  ngOnInit() {
    console.log("here into contact")
    this.count=150
  }

  setCount(msg: any) {
    console.log(msg)
    this.count = this.count + 1;
    
  }
}
