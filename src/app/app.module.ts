import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { HeroComponent } from './components/hero/hero.component';
import { ScoreComponent } from './components/score/score.component';
import { NextMatchComponent } from './components/next-match/next-match.component';
import { ContactComponent } from './components/contact/contact.component';
import { LatestNewsComponent } from './components/latest-news/latest-news.component';
import { AddMatchComponent } from './components/add-match/add-match.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { TableMatchesComponent } from './components/table-matches/table-matches.component';
import { DiversComponent } from './components/match/divers.component';
import { PlayersComponent } from './components/players/players.component';
import { TableplayersComponent } from './components/tableplayers/tableplayers.component';
import { TeamsComponent } from './components/teams/teams.component';
import { TableteamComponent } from './components/tableteam/tableteam.component';
import { BannerComponent } from './components/banner/banner.component';
import { MatchesComponent } from './components/matches/matches.component';
import { PipesPipe } from './pipes/pipes.pipe';
import { MatchDetailComponent } from './components/match-detail/match-detail.component';





@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    HeroComponent,
    ScoreComponent,
    NextMatchComponent,
    ContactComponent,
    LatestNewsComponent,
    AddMatchComponent,
    LoginComponent,
    SignupComponent,
    TableMatchesComponent,
    DiversComponent,
    PlayersComponent,
    TableplayersComponent,
    TeamsComponent,
    TableteamComponent,
    BannerComponent,
    MatchesComponent,
    PipesPipe,
    MatchDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,// TDF
    ReactiveFormsModule, //reactive
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
