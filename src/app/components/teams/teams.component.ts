import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TeamsService } from 'src/app/services/teams.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {
  addTeamForm!: FormGroup;
  team: any = {}; // Initialize with an empty team object
  id: any;
  title: string = "Add Team";

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private mService:TeamsService) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get("id");
    if (this.id) {
      this.title = "Update Team";
      this.getTeamById();
    }
  }

  addTeam() {
    let teams = JSON.parse(localStorage.getItem('teams') || "[]");
    if (this.id) {
      if (this.id) {
        this.mService.editTeam(this.team).subscribe();
       }
       else {
        this.mService.addTeam(this.team).subscribe();
   
       }

    localStorage.setItem("teams", JSON.stringify(teams));
    this.router.navigate(["/tableteam"]);
  }}

  generateId(teams: any) {
    if (teams.length === 0) {
      return 1;
    } else {
      let max = 0;
      for (let i = 0; i < teams.length; i++) {
        if (max < teams[i].id) {
          max = teams[i].id;
        }
      }
      return max + 1;
    }
  }

  getTeamById() {
    let teams = JSON.parse(localStorage.getItem('teams') || '[]');
    for (let i = 0; i < teams.length; i++) {
      if (teams[i].id === Number(this.id)) {
        this.team = teams[i];
      }
    }
  }
}
