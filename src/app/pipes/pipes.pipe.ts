import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pipes'
})
export class PipesPipe implements PipeTransform {

  transform(ch:string): any {
    let newCh=""
    for (let i = 0; i < ch.length ; i++) {
      newCh=ch[i]+newCh
  }
  return newCh

}
}
