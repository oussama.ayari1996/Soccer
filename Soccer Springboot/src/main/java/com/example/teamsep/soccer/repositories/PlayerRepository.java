package com.example.teamsep.soccer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.teamsep.soccer.models.PlayerModel;

@Repository
public interface PlayerRepository extends JpaRepository<PlayerModel, Long>{

}
