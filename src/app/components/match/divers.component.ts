import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-divers',
  templateUrl: './divers.component.html',
  styleUrls: ['./divers.component.css']
})
export class DiversComponent implements OnInit {
@Input() match : any
@Output()  newItemEvent = new EventEmitter<any>()
  constructor() { }

  ngOnInit() {
  }
compare(a: any, b: any) {
  if (Number(a) > Number(b)) {
    return ["Win", "green"];
  } else if (Number(a) < Number(b)) {
    return ["Loss", "red"];
  } else {
    return ["Draw", "blue"];
  }
}


  passId(id:any){
console.log("here id match into child com",this.match.id);
this.newItemEvent.emit(this.match.id)


  }
}
